import { Injectable } from '@angular/core';
import {
  BarcodeScannerOptions,
  BarcodeScanner
} from "@ionic-native/barcode-scanner/ngx";

@Injectable({
  providedIn: 'root'
})
export class QrCodeService {
  dados_qr: String;
  valor_qr: {};
  texto_qr: String;
  barcodeScannerOptions: BarcodeScannerOptions;

  constructor(private barcodeScanner: BarcodeScanner) {
    this.dados_qr = "DATA BASE";
    this.barcodeScannerOptions = {
      showTorchButton: true,
      showFlipCameraButton: true
    };
  }
  async scanCode(): Promise<String> {
    await this.barcodeScanner
      .scan()
      .then(barcodeData => {
        this.texto_qr = barcodeData.text;
        this.valor_qr = barcodeData;
      })
      .catch(err => {
        console.log("Error", err);

      });
    return this.texto_qr;


  }

  encodedText() {
    this.barcodeScanner
      .encode(this.barcodeScanner.Encode.TEXT_TYPE, this.dados_qr)
      .then(
        encodedData => {
          console.log(encodedData);
          this.dados_qr = encodedData;
        },
        err => {
          console.log("Error occured : " + err);
        }
      );
  }
}
