import { Injectable } from '@angular/core';
import {
  Plugins, CameraResultType, Capacitor, FilesystemDirectory,
  CameraPhoto, CameraSource
} from '@capacitor/core';
import { Platform } from '@ionic/angular';

const { Camera, Filesystem, Storage } = Plugins;
@Injectable({
  providedIn: 'root'
})
export class PhotoService {
  public fotos: Photo[] = [];
  private fotos_storage: string = "fotos";
  private plataforma: Platform;

  constructor(platform: Platform) {
    this.plataforma = platform;
  }

  public async addImagemGaleria() {
    const foto_capturada = await Camera.getPhoto({
      resultType: CameraResultType.Uri,
      source: CameraSource.Camera,
      quality: 100
    });

    const imagem_salva = await this.salvarImagem(foto_capturada);
    this.fotos.unshift(imagem_salva);
    Storage.set({
      key: this.fotos_storage,
      value: JSON.stringify(this.fotos)
    });
  }
  public async carregarImagensStorage() {
    const lista_fotos = await Storage.get({
      key: this.fotos_storage
    });
    this.fotos = JSON.parse(lista_fotos.value) || [];

    if (!this.plataforma.is('hybrid')) {
      for (let foto of this.fotos) {
        const readFile = await Filesystem.readFile({
          path: foto.caminho,
          directory: FilesystemDirectory.Data
        });

        foto.caminho_web = `data:image/jpeg;base64,${readFile.data}`;
      }
    }
  }
  private async salvarImagem(cameraPhoto: CameraPhoto) {
    const imagem_base64 = await this.leituraBase64(cameraPhoto);
    const nome = new Date().getTime() + '.jpeg';
    const arquivo_salvo =
      await Filesystem.writeFile({
        path: nome,
        data: imagem_base64,
        directory: FilesystemDirectory.Data
      });
    if (this.plataforma.is('hybrid')) {
      return {
        caminho: arquivo_salvo.uri,
        caminho_web: Capacitor.convertFileSrc(arquivo_salvo.uri),
      };
    }
    else {
      return {
        caminho: nome,
        caminho_web: cameraPhoto.webPath
      };
    }
  }

  private async leituraBase64(cameraPhoto: CameraPhoto) {
    if (this.plataforma.is('hybrid')) {
      const file = await Filesystem.readFile({
        path: cameraPhoto.path
      });
      return file.data;
    }
    else {
      const response = await fetch(cameraPhoto.webPath!);

      const blob = await response.blob();

      return await this.converterBolbBase64(blob) as string;
    }
  }

  private converterBolbBase64 = (blob: Blob) => new Promise((resolve, reject) => {
    const reader = new FileReader;
    reader.onerror = reject;
    reader.onload = () => {
      resolve(reader.result);
    };
    reader.readAsDataURL(blob);
  });

  public async deletePicture(photo: Photo, position: number) {
    this.fotos.splice(position, 1);

    Storage.set({
      key: this.fotos_storage,
      value: JSON.stringify(this.fotos)
    });

    const filename = photo.caminho
      .substr(photo.caminho.lastIndexOf('/') + 1);

    await Filesystem.deleteFile({
      path: filename,
      directory: FilesystemDirectory.Data
    });
  }
}


export interface Photo {
  caminho: string;
  caminho_web: string;
}