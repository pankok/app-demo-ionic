import { Component, OnInit } from '@angular/core';
import { QrCodeService } from "../services/qr_code/qr-code.service";
import { Animation, AnimationController } from '@ionic/angular';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  public texto_qr: String;
  constructor(private qrCodeService: QrCodeService) { }

  async lerQr() {
    this.texto_qr = await this.qrCodeService.scanCode();
  }

  gerarQr() {
    this.qrCodeService.encodedText();
  }



}
