export class User {
    private nome: String = "";
    private email: String = "";
    private telefone: number = 0;
    private data_nascimento: String = "";

    constructor(nome?: String, email?: String, telefone?: number, data_nascimento?: String) {
        this.setNome(nome);
        this.setEmail(email);
        this.setTelefone(telefone);
        this.setDataNascimento(data_nascimento);
    }

    public setNome(nome: String): void { this.nome = nome; }
    public setEmail(email: String): void { this.email = email; }
    public setTelefone(telefone: number): void { this.telefone = telefone; }
    public setDataNascimento(data_nascimento: String): void { this.data_nascimento = data_nascimento; }

    public getNome(): String { return this.nome; }
    public getEmail(): String { return this.email; }
    public getTelefone(): number { return this.telefone; }
    public getDataNascimento(): String { return this.data_nascimento; }



}
