import { User } from './../../models/user/user.model';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class FormComponent implements OnInit {
  formulario: FormGroup;
  editar: boolean = false;
  user: User = new User();
  submeter_formulario: boolean = false;

  constructor(public formBuilder: FormBuilder) {
    // chamar servico com dados de usuario
    // this.user = new User("Leonardo A", "leonardop1@gmail.com", 98992037525, "1967-07-11");
  }

  ngOnInit() {
    this.formulario = this.formBuilder.group({
      nome: [this.user.getNome(), [Validators.required, Validators.minLength(2)]],
      email: [this.user.getEmail(), [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
      telefone: [this.user.getTelefone(), [Validators.required, Validators.pattern('^[0-9]+$')]],
      data_nascimento: [this.user.getDataNascimento()],
    })
  }

  pegarData(data_evento) {
    let data = new Date(data_evento.target.value).toISOString().substring(0, 10);
    this.formulario.get('data_nascimento').setValue(data, {
      onlyself: true
    })
  }

  get controleErros() {
    return this.formulario.controls;
  }

  submeterFormulario() {
    this.submeter_formulario = true;
    if (!this.formulario.valid) {
      return false;
    } else {
      this.user = new User(this.formulario.value.nome, this.formulario.value.email, this.formulario.value.telefone, this.formulario.value.data_nascimento);
      this.alterarModoEdicao();
    }
  }
  alterarModoEdicao() {
    this.editar = !this.editar;
  }
}
